const {
  addProperty,
  getAllProperties,
  getSingleProperty,
  updateProperty,
  deleteProperty,
  uploadImage,
} = require("../controller/propertyController");
const { authenticatedUser } = require("../middleware/authenticated");
const express = require("express");

const router = express.Router();

// the authenticatedUser middleware is used for routes that requires request from only signed in users
router.route("/add-property").post(authenticatedUser, addProperty);
router.route("/upload-image").post(authenticatedUser, uploadImage);
router.route("/properties").get(getAllProperties);
router.route("/get-property/:id").get(getSingleProperty);
router.route("/update-property/:id").patch(authenticatedUser, updateProperty);
router.route("/delete-property/:id").delete(authenticatedUser, deleteProperty);

module.exports = router;
