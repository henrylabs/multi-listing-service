const express = require("express");
const authorizedUser = require("../middleware/authenticated");
const {
  register,
  verifyEmail,
  login,
  logout,
  resetPassword,
  forgotPassword,
} = require("../controller/authController");

const router = express.Router();

router.post("/register", register);
router.post("/verify-email", verifyEmail);
router.post("/login", login);
router.get("/logout", logout);
router.post("/reset-password", resetPassword);
router.post("/forgot-password", forgotPassword);
module.exports = router;
