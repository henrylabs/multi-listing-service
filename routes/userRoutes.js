const {
  getAllUsers,
  getSingleUser,
  deleteUser,
  getUserProperty,
  getCurrentUser,
} = require("../controller/userController");
const { authenticatedUser } = require("../middleware/authenticated");
const express = require("express");

const router = express.Router();

router.route("/users").get(getAllUsers);
router.route("/user-property").get(authenticatedUser, getUserProperty);
router.route("/current-user").get(authenticatedUser, getCurrentUser);
router.route("/user/:id").get(getSingleUser);
router.route("/delete-user/:id").delete(authenticatedUser, deleteUser);

module.exports = router;
