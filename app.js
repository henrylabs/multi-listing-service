const express = require("express");
const cookieParser = require("cookie-parser");
const authRouter = require("./routes/authRoutes");
const propertyRouter = require("./routes/propertyRoutes");
const userRouter = require("./routes/userRoutes");
const errorHandler = require("./middleware/error-handler");
const notFound = require("./middleware/not-found");
require("dotenv").config();
const cloudinary = require("cloudinary").v2;
const fileUpload = require("express-fileupload");
const helmet = require("helmet");
const xss = require("xss-clean");
const cors = require("cors");
const mongoSanitize = require("express-mongo-sanitize");

const connectDB = require("./db/connect");

const app = express();

// to parse json requests
app.use(express.json());
app.use(express.static("./public"));

// for file uploads
app.use(fileUpload({ useTempFiles: true }));

// to use signed cookies by making use of the json web token secret
app.use(cookieParser(process.env.JWT_SECRET));

app.set("trust proxy", 1);
app.use(helmet());
app.use(cors());
app.use(xss());
app.use(mongoSanitize());

// url prefixes
app.use("/api/v1/auth", authRouter);
app.use("/api/v1", propertyRouter);
app.use("/api/v1", userRouter);

// cloudinary API connection setup
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_API_KEY,
  api_secret: process.env.CLOUD_API_SECRET,
});

// sample route
app.get("/", (req, res) => {
  res.send("Multi Listing Services");
});

app.use(errorHandler);
app.use(notFound);

const port = process.env.PORT || 3000;

// connecting the database and starting the server
const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL);
    console.log("DB Connected!");
    app.listen(port, () =>
      console.log(`Server listening on port ${port} . . .`)
    );
  } catch (error) {
    console.log(error);
  }
};

start();
