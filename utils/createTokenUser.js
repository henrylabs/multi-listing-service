// payloads used in generating user token
const createTokenUser = (user) => {
  return {
    name: user.name,
    userID: user._id,
    email: user.email,
    role: user.role,
  };
};

module.exports = createTokenUser;
