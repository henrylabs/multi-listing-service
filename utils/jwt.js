const jwt = require("jsonwebtoken");

// creating the token
const createJWT = (payload) => {
  const token = jwt.sign(payload, process.env.JWT_SECRET);
  return token;
};

const verifyJWT = (token) => jwt.verify(token, process.env.JWT_SECRET);

// to attach cookie to responses
const attachCookieToResponse = ({ res, userToken }) => {
  const token = createJWT({ payload: userToken });
  const oneDay = 1000 * 60 * 60 * 24;
  res.cookie("token", token, {
    httpOnly: true,
    secured: process.env.NODE_ENV === "production",
    signed: true,
    expires: new Date(Date.now() + oneDay),
  });
};

module.exports = { verifyJWT, attachCookieToResponse };
