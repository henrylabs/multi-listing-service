const nodemailer = require("nodemailer");
const nodemailerConfig = require("./nodemailerConfig");

// for sending email messages to users
const sendEmail = async ({ to, subject, html }) => {
  let testAccount = await nodemailer.createTestAccount();
  const transporter = nodemailer.createTransport(nodemailerConfig);

  // send mail with defined transport object
  return transporter.sendMail({
    from: '"Henry Omeja" <henry.omeja@romanzeus.com>', // sender address
    to, // list of receivers
    subject, // Subject line
    html, // html body
  });
};

module.exports = sendEmail;
