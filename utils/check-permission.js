const CustomError = require("../errors/index");

// to prevent a user from deleting another user's account, the function checkPermission() helps make sure that the user making the request is either a user with an admin role or the same user making the request is deleting his own account

const checkPermission = (requestUser, resourceID) => {
  if (requestUser.role === "admin") return;
  if (requestUser.userID === resourceID.toString()) return;
  throw new CustomError.BadRequestError("Bad Request");
};

module.exports = checkPermission;
