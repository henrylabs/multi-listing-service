const sendEmail = require("./sendEmail");

const sendVerificationMail = ({ name, email, verificationToken, origin }) => {
  const verifyEmail = `${origin}/user/verify-email?token=${verificationToken}&email=${email}`;
  const message = `<p>Please verify your email by clicking on the following link <a href=${verifyEmail}>Verify Account</a></p>`;
  return sendEmail({
    to: email,
    subject: "Email Confirmation",
    html: `Dear ${name},
        ${message}`,
  });
};

module.exports = sendVerificationMail;
