// for incorrect or invalid url request - returns a 404 response
const notFound = (req, res) => {
  res.status(404).send("Route not found");
};

module.exports = notFound;
