const { verifyJWT } = require("../utils/jwt");
const customError = require("../errors/index");

// a middleware to make sure a user is signed in before making any request that requires only registered user to execute
const authenticatedUser = (req, res, next) => {
  // destructuring the token from the request cookies
  const { token } = req.signedCookies;

  // if no token is found - meaning the request is coming from an unregistered user, it returns an error
  if (!token) {
    throw new customError.UnauthenticatedError("Unauthentication Error");
  }
  try {
    // creating a payload from the token
    const payload = verifyJWT(token);
    req.user = {
      name: payload.payload.name,
      userID: payload.payload.userID,
      email: payload.payload.email,
      role: payload.payload.role,
    };
    next();
  } catch (error) {
    console.log(error);
  }
};

module.exports = { authenticatedUser };
