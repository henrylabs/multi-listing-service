const User = require("../model/User");
const { StatusCodes } = require("http-status-codes");
const customError = require("../errors/index");
const crypto = require("crypto");
const { attachCookieToResponse } = require("../utils/jwt");
const createTokenUser = require("../utils/createTokenUser");
const sendVerificationMail = require("../utils/sendVerificationMail");
const sendResetPassword = require("../utils/sendResetPasswordEmail");

const register = async (req, res) => {
  const { name, number, office_location, password, email } = req.body;
  // to automatically give the first registered account the 'admin' role and subsequent accounts 'user' roles
  const firstPerson = (await User.countDocuments({})) === 0;
  const role = firstPerson ? "admin" : "user";
  // generate code for verification token
  const verificationToken = crypto.randomBytes(40).toString("hex");
  const user = await User.create({
    name,
    number,
    office_location,
    password,
    email,
    verificationToken,
    role,
  });
  // generated json web token
  const userToken = createTokenUser(user);
  // a cookie is created as response for every user account
  attachCookieToResponse({ res, userToken });
  const origin = "http://localhost:3000";
  await sendVerificationMail({
    name: user.name,
    email: user.email,
    verificationToken: user.verificationToken,
    origin,
  });
  res.status(StatusCodes.CREATED).json({
    msg: "Account created successfully, please check your email and verify your account",
  });
};

const verifyEmail = async (req, res) => {
  const { verificationToken, email } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    throw new customError.NotFoundError(
      "This email is not attached to any account"
    );
  }
  // to make sure the verification code entered in the request body is the same as what is on the database
  if (user.verificationToken !== verificationToken) {
    throw new customError.BadRequestError("Invalid Verification");
  }
  // after user verifies account, attributes gets updated
  user.isVerified = true;
  user.verified = Date.now();
  user.verificationToken = "";
  await user.save();
  res.status(StatusCodes.OK).json({ msg: "Email Verified" });
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    throw new customError.NotFoundError(
      "This email is not attached to any account"
    );
  }
  // we compare the password entered in the request body to the mongoose comparePassword method
  const isPasswordCorrect = await user.comparePassword(password);
  console.log(password);
  if (!isPasswordCorrect) {
    throw new customError.BadRequestError("Invalid authentication");
  }
  // to be sure the logged in user account is verified
  if (!user.isVerified) {
    throw new customError.BadRequestError("Please verify your account");
  }
  const userToken = createTokenUser(user);
  attachCookieToResponse({ res, userToken });
  res.status(StatusCodes.OK).json({ user: userToken });
};

const logout = async (req, res) => {
  res.cookie("token", "logout", {
    httpOnly: true,
    expires: new Date(Date.now()),
  });
  res.status(StatusCodes.OK).json({ msg: "User logged out!" });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (user) {
    const passwordToken = crypto.randomBytes(70).toString("hex");
    const origin = "http://localhost:3000";
    await sendResetPassword({
      name: user.name,
      email: user.email,
      token: passwordToken,
      origin,
    });
    // converting milliseconds to 10 minutes
    const tenMinutes = 1000 * 60 * 10;
    // setting the token expiry date
    const passwordTokenExpirationDate = new Date(Date.now() + tenMinutes);
    // updating the attributes in the database
    user.passwordToken = passwordToken;
    user.passwordTokenExpirationDate = passwordTokenExpirationDate;
    await user.save();
  }
  res
    .status(StatusCodes.OK)
    .json({ msg: "Password reset link sent to your email" });
};

const resetPassword = async (req, res) => {
  const { email, password, token } = req.body;
  const user = await User.findOne({ email });
  if (user) {
    const currentDate = new Date();
    // to make the password reset token secured, we make sure the token hasn't expired by checking that the expiry date is more than the current date
    if (
      user.passwordToken === token &&
      user.passwordTokenExpirationDate > currentDate
    ) {
      // update the entries to the database
      user.password = password;
      user.passwordToken = null;
      user.passwordTokenExpirationDate = null;
      // here the instance method is used in saving documents to the database
      await user.save();
    }
  }
  res.status(StatusCodes.OK).json({ msg: "password reset successful!" });
};

module.exports = {
  register,
  verifyEmail,
  login,
  logout,
  forgotPassword,
  resetPassword,
};
