const Property = require("../model/Property");
const { StatusCodes } = require("http-status-codes");
const cloudinary = require("cloudinary").v2;
const fs = require("fs");
const customError = require("../errors/index");
const checkPermission = require("../utils/check-permission");

// adding a new property
const addProperty = async (req, res) => {
  // the marketBy field is the current logged in user
  req.body.marketedBy = req.user.userID;

  // all the fields in the request body gets created in the database
  const property = await Property.create(req.body);
  res.status(StatusCodes.CREATED).json({ property });
};

// get all properties
const getAllProperties = async (req, res) => {
  const property = await Property.find({});
  res.status(StatusCodes.OK).json({ property });
};

// get a single property by its id
const getSingleProperty = async (req, res) => {
  const { id } = req.params;
  const property = await Property.findOne({ _id: id });
  if (!property) {
    throw new customError.NotFoundError(`No property of id ${id} found`);
  }
  res.status(StatusCodes.ACCEPTED).json({ property });
};

// update property
const updateProperty = async (req, res) => {
  const { id } = req.params;
  const property = await Property.findOneAndUpdate({ _id: id }, req.body, {
    new: true,
    runValidators: true,
  });
  if (!property) {
    throw new customError.NotFoundError(`No property of id ${id} found`);
  }

  res.status(StatusCodes.ACCEPTED).json({ property });
};

// delete a property by its id
const deleteProperty = async (req, res) => {
  const { id } = req.params;
  const property = await Property.findOne({ _id: id });
  if (!property) {
    throw new customError.NotFoundError(`No property of id ${id} found`);
  }

  // the instance method is used here to delete a property
  await property.remove();
  res
    .status(StatusCodes.ACCEPTED)
    .json({ msg: `Property with id ${id} removed successfully` });
};

// upload image to a cloud storage (cloudinary) and using the image url created in the image field when adding property
const uploadImage = async (req, res) => {
  const result = await cloudinary.uploader.upload(
    req.files.image.tempFilePath,
    {
      use_filename: true, // the source file name is maintained
      folder: "image-upload", // the image is uploaded to a folder created in cloudinary with the name 'image-upload'
    }
  );
  fs.unlinkSync(req.files.image.tempFilePath);
  res.status(StatusCodes.CREATED).json({ image: { src: result.secure_url } });
};

module.exports = {
  addProperty,
  getAllProperties,
  getSingleProperty,
  updateProperty,
  deleteProperty,
  uploadImage,
};
