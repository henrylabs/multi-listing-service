const User = require("../model/User");
const Property = require("../model/Property");
const checkPermission = require("../utils/check-permission");
const { StatusCodes } = require("http-status-codes");
const CustomError = require("../errors/index");

// get all user accounts
const getAllUsers = async (req, res) => {
  const user = await User.find({}).select("-password");
  res.status(StatusCodes.OK).json({ user });
};

// to find a specific user
const getSingleUser = async (req, res) => {
  const { id: userID } = req.params;
  const user = await User.findOne({ _id: userID });
  if (!userID) {
    throw new customError.NotFoundError(`No user with id ${id} found`);
  }
  res.status(StatusCodes.ACCEPTED).json({ user });
};

// route to get all properties created by a user
const getUserProperty = async (req, res) => {
  const property = await Property.findOne({
    marketedBy: req.user.userID,
  });
  if (!property) {
    throw new CustomError.NotFoundError(`No property of id ${id} found`);
  }
  res.status(StatusCodes.ACCEPTED).json({ property });
};

// get the info of the currently logged in user
const getCurrentUser = (req, res) => {
  res.status(StatusCodes.OK).json({ user: req.user });
};

// deleting a user
const deleteUser = async (req, res) => {
  const { id: userID } = req.params;
  const user = await User.findOne({ _id: userID });
  if (!userID) {
    throw new CustomError.NotFoundError(`No user with id ${userID} found`);
  }

  // to prevent a user fro deleting another user's account, the function checkPermission() helps make sure that the user making the request is either a user with an admin role or the same user making the request is deleting his own account
  checkPermission(req.user, user._id);
  await user.remove();
  res
    .status(StatusCodes.ACCEPTED)
    .json({ msg: `User with id ${userID} removed successfuly` });
};

module.exports = {
  getAllUsers,
  getSingleUser,
  deleteUser,
  getUserProperty,
  getCurrentUser,
};
