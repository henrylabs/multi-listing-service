#!/usr/bin/env bash

apt update -y
apt install ansible -y
apt install  python3-pip -y
apt install awscli -y
pip3 install boto3 botocore
pip3 install openshift PyYAML jsonpatch