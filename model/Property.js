const mongoose = require("mongoose");

// property schema
const PropertySchema = new mongoose.Schema(
  {
    description: {
      type: String,
      maxlength: 200,
      required: true,
    },
    location: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      default: "/uploads/example.jpg",
    },
    price: {
      type: Number,
      required: true,
    },
    featured: {
      type: Boolean,
      default: false,
    },
    market_status: {
      type: String,
      enum: ["Available", "Taken"],
      default: "Available",
    },
    servicing: {
      type: Boolean,
      default: false,
    },
    service_charge: {
      type: Number,
    },
    property_type: {
      type: String,
      enum: ["Terraced", "Duplex"],
      default: "Duplex",
    },
    property_ref: {
      type: Number,
    },
    bedrooms: {
      type: Number,
    },
    bathrooms: {
      type: Number,
    },
    contact_number: {
      type: Number,
      required: true,
    },
    marketedBy: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
    },
    createdAt: {
      type: Date,
      default: new Date(Date.now()),
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Property", PropertySchema);
