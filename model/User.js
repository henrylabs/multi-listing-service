const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    number: {
      type: Number,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      validate: {
        validator: validator.isEmail,
        message: "Please enter a valid email",
      },
      unique: true,
    },
    role: {
      type: String,
      enum: ["admin", "user"],
      default: "user",
    },
    office_location: {
      type: String,
      required: true,
    },
    website: {
      type: String,
    },
    account_type: {
      type: String,
      enum: ["Real Estate Agent", "Property Developers"],
      default: "Real Estate Agent",
    },
    verificationToken: {
      type: String,
    },
    verified: {
      type: Date,
    },
    passwordToken: {
      type: String,
    },
    isVerified: {
      type: Boolean,
      default: false,
    },
    passwordTokenExpirationDate: {
      type: Date,
    },
  },
  { timestamps: true }
);

// the pre save hook to hash user password before saving passwords to the database
UserSchema.pre("save", async function () {
  // to prevent login issues after reseting passwords
  if (!this.isModified("password")) return;
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// method for making sure the password entered in the request body matches whats in the database
UserSchema.methods.comparePassword = async function (userPassword) {
  const passwordMatch = await bcrypt.compare(userPassword, this.password);
  return passwordMatch;
};

module.exports = mongoose.model("User", UserSchema);
